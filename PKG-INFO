Metadata-Version: 2.1
Name: glue-core
Version: 1.0.1
Summary: Multidimensional data visualization across files
Home-page: http://glueviz.org
Author: Thomas Robitaille, Chris Beaumont
Author-email: glueviz@gmail.com
License: UNKNOWN
Description: |Azure Status| |Coverage Status| |DOI| |User mailing list| |Developer mailing list|
        
        Glue
        ====
        
        Glue is a python project to link visualizations of scientific datasets
        across many files. Click on the image for a quick demo:
        
        |Glue demo|
        
        Features
        --------
        
        -  Interactive, linked statistical graphics of multiple files.
        -  Support for many `file
           formats <http://www.glueviz.org/en/latest/faq.html#what-data-formats-does-glue-understand>`__
           including common image formats (jpg, tiff, png), ascii tables,
           astronomical image and table formats (fits, vot, ipac), and HDF5.
           Custom data loaders can also be `easily
           added <http://www.glueviz.org/en/latest/customization.html#custom-data-loaders>`__.
        -  Highly `scriptable and
           extendable <http://www.glueviz.org/en/latest/coding_with_glue.html>`__.
        
        Installation
        ------------
        
        For installation documentation, visit
        `glueviz.org <http://glueviz.org>`__.
        
        Contributing
        ------------
        
        If you are interested in contributing to ``glue``, please read our
        `Code of Conduct <https://github.com/glue-viz/.github/blob/master/CODE_OF_CONDUCT.md>`_
        and `Contribution Guidelines <https://github.com/glue-viz/.github/blob/master/CONTRIBUTING.md>`_.
        
        Support
        -------
        
        Please report problems to glueviz@gmail.com, or `open an
        issue <https://github.com/glue-viz/glue/issues?state=open>`__.
        
        License
        -------
        
        Glue is licensed under the `BSD
        License <https://github.com/glue-viz/glue/blob/master/LICENSE>`__.
        
        .. |Azure Status| image:: https://dev.azure.com/glue-viz/glue/_apis/build/status/glue-viz.glue?branchName=master
           :target: https://dev.azure.com/glue-viz/glue/_build/latest?definitionId=4&branchName=master
        .. |Coverage Status| image:: https://codecov.io/gh/glue-viz/glue/branch/master/graph/badge.svg
           :target: https://codecov.io/gh/glue-viz/glue
        .. |DOI| image:: https://zenodo.org/badge/doi/10.5281/zenodo.13866.svg
           :target: http://dx.doi.org/10.5281/zenodo.13866
        .. |User mailing list| image:: http://img.shields.io/badge/mailing%20list-users-green.svg?style=flat
           :target: https://groups.google.com/forum/#!forum/glue-viz
        .. |Developer mailing list| image:: http://img.shields.io/badge/mailing%20list-development-green.svg?style=flat
           :target: https://groups.google.com/forum/#!forum/glue-viz-dev
        .. |Glue demo| image:: https://raw.githubusercontent.com/glue-viz/glue/master/doc/readme.gif
           :target: http://vimeo.com/53378575
        
Platform: UNKNOWN
Classifier: Intended Audience :: Science/Research
Classifier: Operating System :: OS Independent
Classifier: Programming Language :: Python
Classifier: Programming Language :: Python :: 3
Classifier: Programming Language :: Python :: 3.6
Classifier: Programming Language :: Python :: 3.7
Classifier: Programming Language :: Python :: 3.8
Classifier: Topic :: Scientific/Engineering :: Visualization
Classifier: License :: OSI Approved :: BSD License
Requires-Python: >=3.6
Provides-Extra: all
Provides-Extra: astronomy
Provides-Extra: docs
Provides-Extra: qt
Provides-Extra: recommended
Provides-Extra: test
